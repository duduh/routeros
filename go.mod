module gitlab.com/duduh/routeros

go 1.12

require (
	github.com/sirupsen/logrus v1.4.2 // indirect
	google.golang.org/grpc v1.23.0 // indirect
)
