package routeros

import (
	"gitlab.com/duduh/routeros/proto"
	"sync"
)
// chanReply is shared between ListenReply and AsyncReply.
type chanReply struct {
	mux sync.Mutex
	tag string
	err error
	reC chan *proto.Sentence
}

// Err returns the first error that happened processing sentences with tag.
func (a *chanReply) Err() error {
	a.mux.Lock()
	defer a.mux.Unlock()
	return a.err
}

func (a *chanReply) close(err error) {
	a.mux.Lock()
	defer a.mux.Unlock()
	if a.err == nil {
		a.err = err
	}
	close(a.reC)
}
