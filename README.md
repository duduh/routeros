# RouterOS Client for the Go language

Go library for accessing Mikrotik devices using the RouterOS API.

Look in the examples directory to learn how to use this library:
[run](examples/run/main.go),
[listen](examples/listen/main.go),
[tab](examples/tab/main.go).

API documentation is available at [godoc.org](http://godoc.org/gitlab.com/duduh/routeros).

Released versions:
[**v2**](https://github.com/go-routeros/routeros/tree/v2)
[**v1**](https://github.com/go-routeros/routeros/tree/v1)
To install it, run:

    go get gitlab.com/duduh/routeros
